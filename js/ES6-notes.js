"use strict";

// ES6

// EXPONENT OPERATOR
// JAVASCRIPT
Math.pow(2,5);
// console.log(Math.pow(2,5)); // 32

// ES6
// console.log(2 ** 5); // 32

// VAR vs LET vs CONST

// let - blocked scoped variable - reassign values
// const- blocked scoped variable - cannot be used to reassign values
// var- global variable -- can be used to reassigned values

let instructor = "Stephen";
instructor = "Justin"; // Justin

// console.log(instructor);

const instructorTwo = "Stephen";
// console.log(instructorTwo); // Stephen

// instructorTwo = "Justin";
// console.log(instructorTwo); // Uncaught Typeerror: Assignment to constant variable
//
// const age = 21;
// age += 1;


// TEMPLATE STRINGS vs CONCATENATION

// concatenation
const cohort = "Delta";
// console.log("Hello, and welcome to " + cohort + "!");

// template strings
// console.log(`Hello, and welcome to ${cohort}!`)


// for... of
// syntax for ... of
/*
for (let element of iterable) {

}
 */

const arr = ["one","two","three"];

for (let e of arr ) {
    // console.log(e);
}


// example above in a for loop?
for (var i=0; i < arr.length; i++) {
    // console.log(arr[i]);
}


// TODO: USING A FOR OF LOOP, LOG EACH DAY OF THE WEEK
const days= ["sunday","monday","tuesday","wednesday","thursday","friday","saturday"];

let daysOnHtml = "";


for (let e of days) {
    // console.log(e)
    // get the days onto the html webpage...
    // let daysOnHtml = "";

    daysOnHtml += `

        <ul>
            <li>${e}</li>
        </ul>
    `;

    // $("#days").append(daysOnHtml);
    $("#days").html(daysOnHtml);
}


// ARROW FUNCTIONS
// - a shorthand function syntax

// js
function sayhello(name) {
    return `Hello ${name}`;
}
// console.log(sayhello("delta"))

// es6 arrow function
let sayHelloAgain = name => `Hello ${name}`;

// console.log(sayHelloAgain("Delta"));


// js function with no parameters
function returntwo() {
    return 2;
}
// console.log(returntwo());

// es6 arrow function with no parameters
let returnTwoAgain = () => 2;


// typeof
var x = 1;
// console.log(typeof x); // number
//
// var y = "Delta";
// console.log(typeof y); // string
//
// var z = true;
// console.log(typeof z); // boolean
//
// var a;
// console.log(typeof a ); // undefined


// DEFAULT FUNCTION PARAMETER VALUES
function greeting(cohort) {
    if (typeof cohort === "undefined") {
        cohort = "World";
    }
    return console.log(`Hello ${cohort}`);
}

greeting(); // Hello World

greeting("Delta"); // Hello Delta


// example above in es6
const greetingAgain = (cohort = "World") => console.log(`hello ${cohort}`);
greetingAgain(); // Hello world
greetingAgain("Echo"); // Hello Echo


//SECOND EXAMPLE OF DEFAULT PARAMETERS
var addArgA = (num1, num2) => {
    if (num1 === undefined) {
        num1 = 2;
    }
    if (num2 === undefined) {
        num2 = 2;
    }
    return num1 + num2;
};
//
// console.log(addArgA()); // 4
// console.log(addArgA(1)); // 1 + 2  = 3
// console.log(addArgA(5,9)); // 14


// OBJECT PROPERTY VARIABLE ASSIGNMENT SHORTHAND

// objects from javascript
// var person = {
//     name: "Delta",
//     age: 2021
// };

// objects from es6 shorthand
// const name = "Delta";
// const age = 2021;
//
// const person = {
//     name,
//     age
// };


// OBJECTS DESTRUCTURING
// - shorthand for creating variables from object properties

// javascript
// var person = {name:"Delta", age: 2};
// var personName = person.name; // delta
// var personAge = person.age; 2

// es6
const person = {name:"Delta", age: 2};
const {name,age} = person;


// DESTRUCTURING ARRAYS
var myArray = [1,2,3,4,5];
const [a, y, thirdIndex, bob, adam] = myArray;

console.log(bob); // 4


//==============================================
// ARROW FUNCTION DRILLS

// TODO: create a arrow function that'll take in a length and width
// and returns the area

// js version
function area(length, width) {
    return length * width;
}
console.log(area(10,20)); // 200

// arrow version
const areaAgain = (length, width) => length * width;
console.log(areaAgain(10,2)); // 200


// TODO: create a arrow function that'll take in a length and width
// and returns the perameter

const anotherArea = (width, height) => (2 * width) + (2 * height);
console.log(anotherArea(6,2));


// TODO: refactor the following function to an arrow
function helloCohort(greeting, cohort) {
    if (typeof greeting === "undefined"){
        greeting = "delta";
    }

    if (typeof cohort === "undefined") {
        cohort = "delta";
    }

    return greeting + " " + cohort;
}


const helloCohortAgain = (greeting = "Good Day", cohort ="delta") => `${greeting} ${cohort}`;
console.log(helloCohortAgain());