"use strict";

// Jquery document ready
$(document).ready(function () {
    alert("This page has finished loading...")
});

// JAVASCRIPT
// window.onload = function () {
//     alert("this page has finished loading...")
// }


// JQuery selectors
// syntax: $("selectors")

// ID selector
var content = $("#codebound").html(); // .html() --> similar to .innerHTML
console.log(content);


// CSS Selector | .css() --> similar to .style property
$(".urgent").css("background-color", "red") // single property

$(".not-urgent").css(
    {
        "background-color": "yellow",
        "text-decoration": "line-through"
    }
)
// MULTIPLE STYLE PROPERTYS ^

// MULTIPLE SELECTOR
$(".list").css("color","orange");

// ALL SELECTOR
$("*").css("background-color", "green");

// ELEMENT SELECTOR
$("h1").css("text-decoration", "underline");