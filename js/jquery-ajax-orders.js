"use strict";

// created variable and assign a ajax GET request to data/orders.json
var ajaxRequest = $.ajax("data/orders.json");

// callback methods for handling responses
/*
.done()
.fail()
.always()
 */

ajaxRequest.done(function (data) {
    // gets the entire array of objects
    console.log(data);

    // // gets the first object of the array
    // console.log(data[0]);
    //
    // // get the price property of the first object of the array...
    // console.log(data[0].price);
    //
    // // get the value of "Cap"...
    // console.log(data[3].item);
    //
    // // get the value of "Alyssa"
    // console.log(data[2].orderedBy);
    //
    // // get the value of 12345
    // console.log(data[1].orderNum);

    // create a local variable and assign the buildHTML() function to it...
    var dataToHTML = buildHTML(data);

    // target our id "orders" in the html , and render our data
    $("#orders").html(dataToHTML);
});





// create a function that will display our "data" from our request...
function buildHTML(orders) {
    // create a local variable and assign amd empty string...
    var orderHTML = "";

    // loop through the array for each individual order
    // orders.forEach(function (order) {
    //     orderHTML += "<section>";
    //     orderHTML += "<dl>";
    //     orderHTML += "<dt>" + "Ordered Item" + "</dt>";
    //     orderHTML += "<br>";
    //     orderHTML += "<dd>" + order.Item + "</dd>";
    //     orderHTML += "</dl>";
    //     orderHTML += "</section>";
    // });

    // template strings example
    orders.forEach(function (order) {
        orderHTML += `
        <h3>GENRE</h3>
        <p> ${order.Item} </p>
        <p> Ordered By: ${order.orderedBy}</p>
      
        `;


    });


    return orderHTML;
}