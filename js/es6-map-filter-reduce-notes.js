"use strict";

/*
MAP, FILTER, REDUCE!

MAP: TRANSFORM EACH ELEMENT IN THE COLLECTION

FILTER: FILTER THE VALUES

REDUCE: REDUCE A COLLECTION TO A SINGLE VALUE
 */

var numbers = [11,20,33,40,55,60,77,80,99,100];

var evens = [];
var odds = [];

// JS FOR LOOP
for (var i = 0; i < numbers.length; i++) {

    // find all the even numbers
    if (numbers[i] % 2 === 0) {
        // add the index from numbers to evens
        evens.push(numbers[i]);
    }
    else {
        odds.push(numbers[i]);
    }
}
// console.log(evens);
// console.log(odds);


// same thing, with filter
var evensFiltered = numbers.filter(function (n) {
    return n % 2 === 0;
})
// console.log(evensFiltered);

var oddsFiltered = numbers.filter(function (n) {
    return n % 2 !== 0;
})
// console.log(oddsFiltered);


// MAP
// WRITE CODE THAT PRODUCES A NEW ARRAY OF NUMBERS WITH
// EACH NUMBER MULTIPLIED BY 10

// var numberTimesTen = numbers.map(function (num) {
//     return num * 10;
// });

const numberTimesTen = numbers.map(num => num * 10);


// console.log(numberTimesTen);


// REDUCE
const myArray = [1, 2, 3, 3, 4, 5];

// const sum = myArray.reduce(function (acc, currentNumber) {
//     return acc + currentNumber;
// }, 0);

//  same example , convert to arrow function
const sum = myArray.reduce((acc,currentNumber) => acc + currentNumber, 0);

// console.log(sum);


// create an array of objects
const officeSales = [
    {
        name: "Jim Halpert",
        sales: 500
    },
    {
        name: "Dwight Schrute",
        sales: 750
    },
    {
        name: "Ryan Howard",
        sales: 150
    }
];

// todo: using a reduce, get the total amount of sales for the week...
const money = officeSales.reduce((acc,person) => acc + person.sales, 0);
console.log(money);