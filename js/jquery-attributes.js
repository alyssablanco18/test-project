$(document).ready(function () {

// syntax: $("selector")

// adding and removing a class
    // $("#highlight-important").click(function () {
    //     $(".important").addClass("highlighted");
    // });
    //
    // $("#highlight-important").dblclick(function () {
    //     $(".important").removeClass("highlighted");
    // });

 // toggle class
 // $("#highlight-important").click(function (e) {
 //     e.preventDefault();
 //     $(".important").toggleClass("highlighted");
 // });

 // e.preventDefault()
 // event.preventDefault()
 /*
 this will stop the default behavior of the anchor tag
 element from firing.
  */

  // challenge:
  // convert the center alignment back to its
  // normal alignment, using jquery.

    $("#highlight-important").click(function (e) {
        e.preventDefault();
        $(".important").toggleClass("centered");
    });


});