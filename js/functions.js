// var count = "input"; {
//     console.log( input.length);
//     console.log( input.substring(0,5));
// }
//
// count()

// Stephens solution
function count(input) {
    console.log(input.length);
}

count("Delta") // 5

// ANGELAS SOlUTION
var myString = " My pets name is blue"; // GlOBAL VARIABLE- variable outside of a function
function count(input) {
    console.log(input.length)
}

count(myString); // 21


function add(a,b) {
    console.log(a + b);
}

add(12,5); //17

function subtract(a,b) {
    console.log(a - b);
}

subtract(20, 12);

function multiply(a,b) {
    console.log( a * b );
}

multiply(10,4 );

function divide( numerator, denominator) {
    console.log( numerator/ denominator);
}

divide(20, 2); //10

function remainder( number, divisor ) {
   console.log( number % divisor);
}

remainder(20, 6); //2

function square(num) {
    // console.log(num ** 2); // ES6 JavaScript
    console.log(num * num);
}

square(5); // 25

// ANONYMOUS FUNCTIONS
// Functions Without A Name and stores it in a variable
/*
SYNTAX:

var name Of Variable = function(){
    ... code inside of the functions body
    };

 */

// SQUARE ANONYMOUS FUNCTION
var square = function (num) {
    console.log(num * num);
};

var result7th = square(5);

// write a function named is Odd(x)
//check whether a number is odd or even
// return true if the number is odd, false if the number is even


function isOdd(x) {
    console.log(x % 2 != 0);
    // 18 % 2 ? does that remainder is NOT zero? true or false
}

isOdd(18);
//does not matter the number


// write a function named is isEven(x)
//check whether a number is odd or even
// return true if the number is even, false if the number is odd


function isEven(eight) {
    console.log(eight % 2 == 0);
    // 5 % 2 have a remainder of 0 ? true or false ?
}

isEven(13);

// console.log(4 / 2);  // 2
// console.log( 4 % 2); // 0
// console.log(5 % 2);  // 1
// console.log(6 % 2); // 0
// three groups of 2
//
// function isSame(input) {
//     console.log(input % 2 == 0);
//    
// }

function isSame(input) {
    var num1 = 10;
    console.log(num1 === input);
    // 10 is the same as "10"
}

isSame("10");


function isSeven(input) {
//     var match = 7
// console.log( input === match);
    console.log(input === 7);
}

isSeven(7);


function addTwo(x) {
    console.log(x + 2);
}
addTwo(4);

function isMultipleOfTen(x) {
    console.log(x % 10 ===0);
}
isMultipleOfTen(100);

function isMultipleOfTwo(x) {
    console.log(x % 2 === 0);
}
isMultipleOfTwo(4);

function isMultipleofTwoAndFour(x) {
    return x % 4 === 0 && x % 2 === 0;
}

console.log(isMultipleofTwoAndFour(4));

function isTrue(input) {
    // JOSES SOLUTION
    // var match = "true";
    // console.log( input === match);
    //               "hello" === "true"

     // ALTERNATE SOLUTION
    console.log(input === "true");
}

isTrue("true");

function isFalse(x) {
    console.log(x === "false");
}

isFalse("false");


function isVowel(input) {
        console.log(input === 'a' || input==='e' || input=== 'i' || input=== 'o'|| input==='u'
            || input=== 'A' || input=== 'E' || input==='I' || input=== 'O' || input=== 'U');
    }

isVowel('A');

function triple(x) {
    console.log( x * 3);
}

triple(15);

function quadNum(x) {
    console.log( x * 4);
}

quadNum(12);
//  Formula radians * ( 180 / pi )
function degreesToRadians(x) {
    // var pi = 3.14;

    // JAVASCRIPT pi: Math.pi
    return x * ( Math.PI / 180 );
}

console.log(degreesToRadians(45));

function reverseString(input) {
    var splitString = input.split(""); // "H", "e", "l", "l", "o"

    var reverseOrder = splitString.reverse(); // "o","l", "l", "e" ,"H"

    var reverseWord = reverseOrder.join(""); // olleH

    return reverseWord;
}

console.log(reverseString("Delta"));



