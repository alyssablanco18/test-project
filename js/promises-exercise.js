(function () {

"use strict";

// CREATE A FILE NAMED 'promises-exercise.js' INSIDE OF YOUR JS DIRECTORY

// Exercise 1:
// Write a function name 'wait' that accepts a number as a parameter, and returns
// a PROMISE that RESOLVES AFTER THE PASSED NUMBER OF MILLISECONDS
    // EX. wait(4000).then(() => console.log('You will see this after 4 seconds.'));
    // EX. wait(8000).then(() => console.log('You will see this after 8 seconds.'));

    // // STEPHENS SOLUTION
    // const wait = (delay) => new Promise((resolve, reject) => {
    //     setTimeout(() =>{
    //         resolve(`You will see this after ${delay / 1000} seconds`)
    //     }, delay);
    // })
    // wait(4000).then((data)=> console.log(data));
    //
    // // #2
    // // function waitMessage(delay) {
    //     return new Promise((resolve, reject) => {
    //         setTimeout( () => {
    //             resolve('you will see this $')
    //         }
    //     })
    // }

    // VICTORS SOLUTION
//     const wait = num => {
// // parameter is ${type}, promise makeOrder
//         let makeOrder = new Promise((resolve, reject) => {
//             setTimeout(function () {
//                 resolve(`${num}`);
//             }, 4000);
//         });
//         return Promise.all([makeOrder]);
//     }
//     wait("4").then((data) => {
//         console.log("You will see this after 4 seconds.");
//     }).catch((error) => console.log(error));


    // ANGELAS SOLUTION
    // function wait() {
    //     return new Promise((resolve, reject) => {
    //         setTimeout(()=>{
    //             if (Math.random()> 0.1){
    //                 resolve("Promises, Promises...");
    //             }
    //             else{
    //                 reject("Broken Promises! smh");
    //             }
    //         }, 1000);
    //     });
    // }
    // const request = wait();
    // console.log(request);



// Exercise 2:
// Write a function testNum that takes a number as an argument and
// returns a Promise that tests if the value is less than or greater than
// the value 10.
//     const testNum = new Promise((resolve, reject) => {
//         let num = 8
//         if ( num < 10) {
//             resolve("Less than")
//         }else {
//             reject("Greater than")
//         }
//         return num;
//     });
//     console.log(testNum);

    // JOSES SOLUTION
    // var x = 10;
    // var testNum = new Promise(function (resolve, reject) {
    //     if (x < 10) {
    //         resolve(x); // call the first .then()
    //     }
    //     else {
    //         reject(x); // call the first catch
    //     }
    // });
    // testNum.then(function (ex) {
    //     console.log(`${x} is less than 10.`); // called by the first resolve()
    // }).catch(function () { // called by the first reject()
    //     var testNum1 = new Promise(function (resolve, reject) {
    //         if (x > 10) {
    //             resolve(x); // call the second .then()
    //         }
    //         else {
    //             reject(x); // call the second .catch()
    //         }
    //     });
    //     testNum1.then(function (ex) {
    //         console.log(`${x} is greater than 10.`); // called by the second resolve()
    //     }).catch(function () {
    //         console.log(`${x} is equal to 10.`); // called by the second reject


// Exercise 3:
//     Write two functions that use Promises that you can chain! The first function,
//     makeAllCaps(), will take in an array of words and capitalize them, and then the
//     second function, sortWords(), will sort the words in alphabetical order. If the array
//     contains anything but strings, it should throw an error.
// const myArray = ["apples","oranges","grapes","bananas"];
//
// const makeAllCaps = array => new Promise((resolve, reject) => {
//     let arrayCaps = array.map(word => {
//     if (typeof word === "string") {
//     }
//
//
// }))

// ANGELAS SOLUTION
    let fish = ['goldfish', 'tuna', 'shark', 'puffer']
    const makeAllCaps = (words) =>
        new Promise ((resolve, reject) => {
            if (words.every(word => typeof word === 'string')){
                resolve(words.map(word => word.toUpperCase()))
            } else {
                reject(Error('No, that was not a string!'))
            }
        });
    const sortWords = (words) => {
        return words.sort((a, b) => {
            if (a > b){
                return 1
            } else {
                return -1
            }
        })
    }
    makeAllCaps(fish)
        .then(words => sortWords(words))
        .then(result => console.log(result))
        .catch(error => console.log(error));
    // const myArray = ["apples", "oranges", "grapes", "bananas"];
    // const makeAllCaps = array => new Promise((resolve, reject) => {
    })