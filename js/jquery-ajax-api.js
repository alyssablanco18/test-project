"use strict";

// load users in our console log
$.get("https://jsonplaceholder.typicode.com/users",{

}).done(function (data) {
    // what we want to do with the data...?
    console.log(data);

    console.log(data.name); // undefined

    // get nicholas runolfsdottir V
    console.log(data[7].name);

    // get "Gwenborough" from the first object of the array...
    console.log(data[0].address.city);

    // create a variable and assign to our function "displayUsers"
    var renderData = displayUsers(data);

    // target our id "users" and append renderData variable
    $("#users").html(renderData);


});

function displayUsers(users) {

    // local variable and assign it to a an empty string
    var usersOnHtml = "";

    // loop through the array
    users.forEach(function (user) {

        // call the local variable and
        // dynamically create the the html

        usersOnHtml += `
        <div class="user">
            <h3>Employee Name: ${user.name}</h3>
            
            <a href="http://${user.website}">VIEW WEBSITE</a>
            
            <p>User Info:</p>
            
            <ul>
                <li>
                    <p>Username: ${user.username}</p>
                </li>
                <li>
                    <p>Address: ${user.address.street} ${user.address.city}</p>
                </li>
            </ul>
        </div>
        `
    })

    // return statement to return our variable "userOnHTML"
    return usersOnHtml;
}