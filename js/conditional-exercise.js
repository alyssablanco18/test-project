"use strict";
// defines that js code should be executed in
// " strict mode"

/**
 * TODO:
 * Write some JavaScript that uses a 'confirm' dialog to ask the user if they would like to enter a number.
 * If they click 'Ok', prompt the user for a number, then use 3 separate alerts to tell the user:
 * - Whether the number is even or odd
 * - What the number plus 100 is
 * - If the number is negative or positive
 * If the user doesn't enter a number, use an alert to tell them that,
 * and do not display any of the information above
 **/
/**
 * TODO:
 * Create a function named 'color' that accepts a string that is a color name as an input.
 * This function should return a message that related to that color. Only worry about the colors defined below,
 * if the color passed is not one of the ones defined below, return a message that says so.
 * Ex
 * color('blue') // returns "blue is the color of the sky"
 * color('red') // returns "Roses are red"
 * You should use an if-else if- else block to return different messages.
 */
/**
 * TODO:
 * It's the year 2021, it's fall, and it's safe to shop again!
 * Suppose there's a promotion at Target, each customer is given a randomly generated
 * "lucky Target number" between 0 and 5. If your lucky number is 0, you have no discount,
 * if your lucky number is 1 you'll get a 10% discount, 2 is a 25% discount, 3 is a 35% discount,
 * 4 is a 50% discount, and 5 you'll get a 75% discount.
 * Write a function named 'calculateTotal' that accepts a lucky number and total amount, and returns the discounted price.
 * Ex
 * calculateTotal(0, 100) // returns 100
 * calculateTotal(4, 100) // return 50
 * Test your function by passing it various values and checking for the expected return value.
 */
/**
 * TODO:
 * Create a grading system using if/else if/ else statements
 // BONUS Use the switch statements
 // 100 - 97 A+
 // 96 - 94 A
 // 93 - 90 A -
 // 89 - 87 B +
 // 86 - 84 B
 // 83 - 80 B -
 // 79 - 77 C +
 // 76 - 74 C
 // 73 - 70 C -
 // 69 - 67 D +
 // 66 - 64 D
 // 63 - 60 D -
 // < 59 = F
 */
/**
 * TODO:
 * Create a time system (MILITARY TIME) using if/else if/else statement
 * OR switch case
 * < 12 = 'Good morning'
 * < 18 = 'Good afternoon'
 * something for 'Good evening
 **/
/**
 * TODO
 * Create a season system using if/else if/else statement
 * EX. 'December - February = Winter', etc.
 **/

// CONFIRM DIALOG!!!
// if (confirm("Would You like To Enter A Number?") === true) {
//
//
//     var input = prompt("enter a number");
//     //  if the input is not a number...
//
//     // alert(input);
//     // checks to see if input is even or odd
//     if(input % 2 === 0){
//         alert(input + " is an even");
//     }
//     else{
//         alert(input + " is an odd");
//     }
//
//
//     // takes input and adds 100 to it...
//     alert(input + " + 100 is " + (Number(input) + 100));
//
//     //checks to see if input is negative or positive
//     if (input >=0) {
//         alert(input + " is a positive number");
//     }
//     else{
//         alert(input + " is a negative number");
//     }
//
// }
// else {
//     alert("Okay, good-bye");
// }



// COLOR!!!
// function color(input) {
//     if (input === "blue"){
//         alert("Blue is the color of the sky");
//     }
//     else if (input === "red"){
//         alert("Roses are red")
//     }
//     else{
//         alert("That is a unique color");
//     }
// }
//
// // CALL A FUNCTION
// // color("blue");
// // color("red");
// color("yellow");



// 2021!!!
// function calculateTotal(luckyNumber, totalAmount) {
//
//     if (luckyNumber === 0){
//         alert(totalAmount);
//     }
//     else if(luckyNumber === 1){
//         alert(totalAmount * .90);
//         // alert(totalAmount - (totalAmount * .10));
//     }
//     else if(luckyNumber === 2){
//         alert(totalAmount * .75);
//     }
//     else if(luckyNumber === 3){
//         alert(totalAmount * .65);
//     }
//     else if(luckyNumber === 4){
//         alert(totalAmount * .50);
//     }
//     else if(luckyNumber === 5){
//         alert(totalAmount * .25);
//     }
// }
// // call our function
// calculateTotal(4, 24);


// GRADE SYSTEM!!!
// var gradeSystem= 0;
//     if (gradeSystem >=97){
//         console.log("A+!");
//     }
//     else if (gradeSystem >=94 && gradeSystem <=96){
//         console.log("A");
//     }
//     else if (gradeSystem >=90 && gradeSystem <=93){
//         console.log("A-");
//     }
//     else if (gradeSystem >=87 && gradeSystem <=89){
//         console.log("B+");
//     }
//     else if (gradeSystem >=84 && gradeSystem <=86){
//         console.log("B");
//     }
//     else if (gradeSystem >=80 && gradeSystem <=83){
//         console.log("B-");
//     }
//     else if (gradeSystem >=77 && gradeSystem <=79){
//         console.log("C+");
//     }
//     else if (gradeSystem >=74 && gradeSystem <=76){
//         console.log("C");
//     }
//     else if (gradeSystem >=70 && gradeSystem <=73){
//         console.log("C-");
//     }
//     else if (gradeSystem >=67 && gradeSystem <=69){
//         console.log("D+");
//     }
//     else if (gradeSystem >=64 && gradeSystem <=66){
//         console.log("D");
//     }
//     else if (gradeSystem >=60 && gradeSystem <=63){
//         console.log("D-");
//     }
//     else {
//         console.log("F");
//     }

// MILITARY TIME

// var input=prompt(" military time");
//
//     if (input === "1200"){
//         console.log("Good Morning!");
//     }
//     else if(input === "1600"){
//         console.log("Good AfterNoon!");
//     }
//     else if(input === "2100"){
//         console.log("Good Evening!");
//     }


// SEASONS
//
// var input=prompt ('Whats your favorite season?');
//
//     if (input === "spring" || input === "Spring"){
//         console.log("March to May");
//     }
//     else if (input === "summer" || input === "Summer"){
//         console.log("June to August");
//     }
//     else if (input === "fall" || input === "Fall"){
//         console.log("September to November");
//     }
//     else if( input === "winter" || input === "Winter"){
//         console.log("December to februrary");
//     }


