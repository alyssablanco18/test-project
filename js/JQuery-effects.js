// add an effect that will fade out the movie list
// in 5 seconds

$("#movie-list-toggle").dblclick(function () {
    $("#movies").fadeOut(5000);
});

// using jquery hide the list of shows
$("#shows").hide();


// using jquery show the list of shows after
// the user hovers over shows on the webpage.
$("#shows-list-toggle").hover(
    function () {
       $("#shows").show();
    },
    function () {
       $("#shows").hide();
    }
)

// 1. in the html, create a element that contains
// "This message appeared after 5 seconds"
// 2. using jquery, make the message appear after 5 seconds,
// when the user hovers onto the document.

// hide the element that contains the message
$("h2").hide();

$(document).hover(function () {
    $("h2").fadeIn(5000);
})
