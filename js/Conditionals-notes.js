// CONDITIONAL STATEMENTS

//IF STATEMENT
/*
SYNTAX
    if ( condition ){
        // code will execute if condition is met
     }
 */

// example
// var numberOfLives = 1; // nothing happens
// var numberOfLives = 0;
// if (numberOfLives === 0) {
//
//     alert( "Game Over!");
//
// }



//  if / else statement
/*
SYNTAX:
    if (condition) {
       code executed if condition is met
     }
     else{
        code executed if condition is not met
     }
 */

// EXAMPLE
// var score = 8;
//
//     if (score > 10){
//         console.log(" Nice job , you scored more than 10!");
//     }
//     else {
//         console.log( "Try again... loser");
//     }

// if /else if / else statement
/*
SYNTAX:
     if ( condition ) {
        code executed if condition is met
      }
      else if ( condition2 ) {
        `code execute if condition is false AND condition2 is true
      }
      else {
        code execute if both conditions are not met
      }
 */

// EXAMPLE
// var age = 11;
//
//     if ( age > 18 && age <  21 ) {
//         console.log( "Welcome to the club, dont drink!");
//     }
//     else if ( age >= 21) {
//         console.log( " Welcome to the club, dont get drunk!");
//     }
//     else {
//         console.log("Beat it Kid!");
//     }

//
// var input = prompt("Whats your favorite fast food?");
//
//     if (input === "pizza" || input === "Pizza") {
//         // "pizza" === "Pizza"
//         console.log(" Cowabunga!");
//     }
//     else if (input === "hamburger") {
//         console.log(" Welcome to Good Burger");
//     }
//     else if ( input === "wings"){
//         console.log("add some hot sauce onto that!");
//     }
//     else {
//         console.log("Eww...");
//     }

// SWITCH STATEMENT NOTES
// another method for writing conditional statements

// EXAMPLE
var color="Blue";

switch (color) {
    case "red":
        alert("You chose red! ");
        break;
    case "Blue":
        alert("You chose blue! ");
        break;
    case "Green":
        alert("You chose green! ");
        break;
    default:
        alert("You didnt select a cool color");
        break;
}
