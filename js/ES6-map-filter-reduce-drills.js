'use strict';

const companies = [
    {name: 'Walmart', category: 'Retail', start_year: 1962, location: { city: 'Rogers', state: 'AR' }},
    {name: 'Microsoft', category: 'Technology', start_year: 1975, location: { city: 'Albuquerque', state: 'NM' }},
    {name: 'Target', category: 'Retail', start_year: 1902, location: { city: 'Minneapolis', state: 'MN' }},
    {name: 'Wells Fargo', category: 'Financial', start_year: 1852, location: { city: 'New York', state: 'NY' }},
    {name: 'Amazon', category: 'Retail', start_year: 1994, location: { city: 'Bellevue', state: 'WA' }},
    {name: 'Capital One', category: 'Financial', start_year: 1994, location: { city: 'Richmond', state: 'VA' }},
    {name: 'CitiBank', category: 'Financial', start_year: 1812, location: { city: 'New York', state: 'NY' }},
    {name: 'Apple', category: 'Technology', start_year: 1976, location: { city: 'Cupertino', state: 'CA' }},
    {name: 'Best Buy', category: 'Retail', start_year: 1966, location: { city: 'Richfield', state: 'MN' }},
    {name: 'Facebook', category: 'Technology', start_year: 2004, location: { city: 'Cambridge', state: 'MA' }},
];

// map all of the company names
// both solutions get the same result
const companyNames = companies.map(function (company) {
    return company.name;
})
// console.log(companyNames);

// ARROW FUNCTION SOLUTION
var compName = companies.map(company => company.name);

// console.log(compName); // outputs the company names


// filter all the company names started before 1990
var years = companies.filter(function (company) {
    return company.start_year <= 1990
})
// console.log(years); // outputs company names before 1990

// filter all the retail companies
const retail = companies.filter(function (r) {
    return r.category === 'Retail';
})
// console.log(retail); // outputs all retail companies


// find all the technology and financial companies
const tech = companies.filter(function (tec) {
    return tec.category === 'Technology'
})
// console.log(tech); // outputs all the technology companies

const finance = companies.filter(function (fine) {
    return fine.category === 'Financial'
})
// console.log(finance) // outputs all the financial companies


// filter all the companies that are located in the state of NY
const ny = companies.filter(function (nyc) {
    return nyc.location.state === 'NY'
})
// console.log(ny); // outputs all the companies located in 'NY'


// find all the companies that started on an even year.
var evensFiltered = companies.filter(function (c) {
    return c.start_year % 2 === 0;
})
// console.log(evensFiltered); // outputs companies on even years



// use map and multiply each start year by 2
const multiply = companies.map(companies => companies.start_year * 2)

// console.log(multiply); // outputs years multiplied by 2



// find the total of all the start year combined.
const start = companies.reduce((acc,year) => acc + year.start_year, 0);

// console.log(start); // outputs total of years


// display all the company names in alphabetical order
var alphaName = companies.map(company => company.name);

// console.log(alphaName.sort()); // outputs in alpha order



// display all the company names for youngest to oldest.
const youngest = companies.map(company => company.name && company.start_year);
console.log(youngest.sort());

const reverseYoungest = youngest.reverse();
console.log(reverseYoungest);

const youngToOld = companies.map(function (company) {
    return company.start_year + "" + company.name;
})
console.log(youngToOld.sort().reverse());

