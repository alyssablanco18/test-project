// EXTERNAL JavaScript

//single line comment

/*
this
is
a
multi
line
comment
 */

// console log
// console.log(5*100);
//
// console.log("hello");
//
// console.log(true);
// console.log(false);


// DATA TYPES
// number, string,boolean, undefined, null...

// VARIABLES
// let, const, var (most common used)

//  DECLARING VARIABLES
// syntax/structure: var nameOfTheVariable;
// syntax/structure: var nameOfVariable = value;

// DECLARE A VARIABLE NAMED FirstName
// var firstname;
//
// // ASSIGN A VALUE OF "Billy" to firstName
// firstname = " Billy";

// CALLED THE VARIABLE NAME
// console.log(firstname);

// DECLARE A VARIABLE NAMED age AND
// ASSIGN A VALUE OF 30 TO age
// var age = 30;
//
// //CALL THE VARIABLE NAME age
// console.log(age);


// OPERATORS
// Mathematical Arithmetic

/*
addition +
subtraction -
multiply *
divide /
MODULUS AKA REMAINDER %
 */

// console.log( 12/4 );
//
// console.log( 12 % 5);
// console.log( 12 % 4);
//
// console.log( (1 + 2) * 4 / 5);
// PEMDAS
// () ^ * / + -

// LOGICAL OPERATORS
/*

AND &&
OR ||
NOT !

NOTE: returns a boolean value when used with boolean values
also used with non-boolean values

AND &&
TRUE STATEMENT && TRUE STATEMENT ( TRUE )
TRUE STATEMENT && FALSE STATEMENT ( FALSE )


 */
// console.log( true && true); // true
// console.log( true && false); // false
// console.log( false && true); // false
// console.log( false && false); // false


// OR ||
// T || F ?
// console.log( true || false); // TRUE
// // T || F ?
// console.log( true || true ); // TRUE
// console.log( false || false); // FALSE
// console.log( false || true ); // TRUE


// NOT !
// the opposite
// !true = false
// !false = true
//
// console.log(!false); // true
//
// console.log(!!!!!!!true);

// COMPARISON OPERATOR
// ==, ===, <, >, <=, >=

// = used to assign values to a variable
// var lastName= " Smith";
//
// //== checks if the VALUE is the same
// //=== checks if the VALUE and the DATA TYPE are the same
//
// // better example of == vs ===
// var entry= 12;
// var entry2 = "12";

// console.log(entry == entry2); // true
// console.log(entry === entry2); // false

// != , !==
// != checks if the VALUE is NOT the same
// !== checks if the VALUE AND DATA TYPE are NOT the same

// console.log(entry != entry2); // false
// console.log(entry !== entry2); // true

// <, >, <=, >=
// const age = 21;

// console.log(age <= 21 || age < 18); // true
// //              T     ||     F     = true
//
// console.log(age <= 21 || age < 18); //true
// //              T            F     = true
//
// console.log( age < 21 && age > 18); // false
//               F    &&     T      = false

// CONCATENATION
const person  = "Bruce";
const person_age = 40;

console.log("Hello my name is " + person);
console.log(" and i am " + person_age  +  " years old ");

console.log(" Hello my name is " + person + " am i am " +  person_age + "  years old ");


// TEMPLATE STRINGS- ` back ticks
console.log(`hello my name is ${ person }`);
console.log(` and i am ${person_age} years old`)

console.log(`Hello my name is ${person} and i am ${person_age} years old.`)

//STRING METHODS
// helps us with... string variables!

// .length()
//GETS THE LENGTH OF A STRING
console.log(person.length); // 5

const greeting = "Hello world!";
console.log(greeting.length) //12


// .toUpperCase() / .toLowerCase()
// GET THE STRING IN UPPERCASE / LOWERCASE
console.log(greeting.toUpperCase()); // HELLO WORLD!
console.log( greeting.toLowerCase()); // hello world!

// .substring()
// extracts character from a string between a "start" and "end"
// index - not including the "end" itself

var goat = "Selena Quintanilla-Perez";
console.log(goat.length); //
console.log(goat.substring(0, 6)); // Selena

// Quintanilla ?
console.log(goat.substring(7, 18)); // Quintanilla

// Perez ?
console.log(goat.substring(19));

//FUNCTION
// - a REUSABLE block  of code that performs
// a specified task (s)



// SYNTAX FOR FUNCTION:
/*
function nameOfFunction() {
     code you want the function to do
     }
 */

// CREATED A FUNCTION name sayHello()
function sayHello() {
    // console.log("Hello");
    alert("Hello");

}

// CALL A FUNCTION
// when we want it to run, we call it by
//its name with the ()
// sayHello();

// CREATE A FUNCTION WITH PARAMETERS
function addNumber(num1) {
    console.log(num1 + 2);
}

// CALL THE FUNCTION ADD NUMBER
addNumber(13);


//CREATE A FUNCTION WITH TWO PARAMETERS
function message(name, age) {
    alert(`Hello my name is ${name} and i am ${age} years old.`);
}

// CALL MESSAGE() with the TWO PARAMETERS
message("Jose", 21);


// CREATE A FUNCTION WITH THREE PARAMETERS
function petInfo(name, animal, age) {
    console.log(" My Pets Name Is " + name + ". " + " He is a " + animal + ", and he is " + age + ".");
}

petInfo( "Chico" , "Bulldog", 7);



