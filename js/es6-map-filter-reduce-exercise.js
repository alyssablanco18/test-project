// MAP FILTER REDUCE EXERCISE
// Complete the following in a new js file name 'map-filter-reduce-exercise.js'

const developers = [
    {
        name: 'stephen',
        email: 'stephen@appddictionstudio.com',
        languages: ['html', 'css', 'javascript', 'angular', 'php'],
        yearExperience: 4
    },
    {
        name: 'karen',
        email: 'karen@appddictionstudio.com',
        languages: ['java', 'javascript', 'spring boot'],
        yearExperience: 3
    },
    {
        name: 'juan',
        email: 'juan@appddictionstudio.com',
        languages: ['html', 'css', 'java', 'aws', 'php'],
        yearExperience: 2
    },
    {
        name: 'leslie',
        email: 'leslie@codebound.com',
        languages: ['node', 'npm', 'sql', 'javascript', 'java' ],
        yearExperience: 5
    },
    {
        name: 'dwight',
        email: 'dwight@codebound.com',
        languages: ['html', 'angular', 'javascript', 'sql'],
        yearExperience: 8
    }
];

/**Use .filter to create an array of developer objects where they have
 at least 5 languages in the languages array*/
const languagesFiltered5 = developers.filter(function (language) {
    if (language.languages.length >= 5) {
        return language;
    }
})
console.log(languagesFiltered5);

/**Use .map to create an array of strings where each element is a developer's
 email address*/
const emailAdd = developers.map(developers => developers.email);

console.log(emailAdd); // outputs the developers emails


/**Use reduce to get the total years of experience from the list of developers.
 * Once you get the total of years you can use the result to calculate the average.*/
var totalExp = developers.reduce((acc,person) => acc + person.yearExperience, 0);

console.log(totalExp); // outputs the total years of the developers- 22

var average = totalExp / developers.length;
console.log(average); // outputs the average- 4.4


/**Use reduce to get the longest email from the list.*/
const longestEmail = developers.reduce((acc, developer) => acc.email.length > developer.email.length ? acc : developer).email;

console.log(longestEmail); // outputs stephens email


/**Use reduce to get the list of developer's names in a single string
 - output:
 CodeBound Staff: stephen, karen, juan, leslie, dwight*/
const names = developers.reduce((acc, developer)=> acc + developer.name, 0);

console.log(names); // outputs the names


// BONUS
/** Use reduce to get the unique list of languages from the list
 of developers
 */
const unique = developers.reduce(function (accumulator,developer) {
    return accumulator + developer.languages;
})
console.log(unique); // outputs all the languages

 const albums = [
     {artist: 'Michael Jackson', title: 'Thrillers', released: 1982, genre: 'Pop'},
     {artist: 'AC/DC', title: 'Back in Black', released: 1980, genre: 'Rock'},
     {artist: 'Pink Floyd', title: 'The Dark Side of the Moon', released: 1973, genre: 'Rock'},
     {artist: 'Bee Gees', title: 'Saturday Night Fever', released: 1977, genre: 'Disco'},
     {artist: 'Fleetwood Mac', title: 'Rumours', released: 1977, genre: 'Rock'},
     {artist: 'Shania Twain', title: 'Come On Over', released: 1997, genre: 'Country'},
     {artist: 'Michael Jackson', title: 'Bad', released: 1987, genre: 'Pop'},
     {artist: 'Led Zeppelin', title: 'Led Zeppelin IV', released: 1971, genre: 'Rock'},
     {artist: 'The Beatles', title: '1', released: 2000, genre: 'Rock'},
     {artist: 'Whitney Houston', title: 'Whitney', released: 1987, genre: 'Pop'},
     {artist: 'Def Leppard', title: 'Hysteria', released: 1987, genre: 'Rock'},
     {artist: 'Tupac', title: 'All Eyez on Me', released: 1996, genre: 'Rap'},
     {artist: 'Eminem', title: 'The Marshall Mathers LP', released: 2000, genre: 'Rap'},
     {artist: 'Green Day', title: 'Dookie', released: 1994, genre: 'Rock'},
     {artist: 'Michael Jackson', title: 'Dangerous', released: 1991, genre: 'Pop'},
     {artist: 'The Notorious B.I.G', title: 'Ready to Die', released: 1994, genre: 'Rap'},
     {artist: 'Adele', title: '21', released: 2011, genre: 'Pop'},
     {artist: 'Metallica', title: 'Load', released: 1996, genre: 'Rock'},
     {artist: 'Prince', title: '1999', released: 1982, genre: 'Pop'},
     {artist: 'Lady Gaga', title: 'Born This Way', released: 2011, genre: 'Pop'}];
/** Create a filter function that logs every 'Pop' album*/
var pop = albums.filter(function (album) {
    return album.genre === "Pop"
})
// console.log(pop); // outputs all 'pop' albums

/**  Create a filter function that logs every 'Rock' album*/
var rock = albums.filter(function (album) {
    return album.genre === 'Rock';
})
// console.log(rock); // outputs all "rock" albums

/** Get the total value of years */
const years = albums.reduce((acc,person) => acc + person.released, 0);
console.log(years) // total of all years

















